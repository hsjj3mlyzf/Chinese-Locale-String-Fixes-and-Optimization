ChinStringFixes = {}

ChinStringFixes.settings = {
		perk_deck_tip = 1,
		others_tip = true,
		Resmod_Compat = true,
		Enable_String = true
	}

ChinStringFixes.values = {
		perk_deck_tip = {
			items = { 
				"chinsf_dialog_ovk", 
				"chinsf_dialog_sig", 
				"chinsf_dialog_ovkplussig", 
				"chinsf_dialog_origin", 
				"chinsf_dialog_LtyR" 
			},
			priority = 10
		},
		others_tip = {
			priority = 11
		},
		Resmod_Compat = {
			priority = 12
		},
		Enable_String = {
			priority = 13
		}
	}

local builder = MenuBuilder:new("chin_string_fixes", ChinStringFixes.settings, ChinStringFixes.values)
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusChinStringFixes", function(menu_manager, nodes)
  builder:create_menu(nodes)
end)


dofile(ModPath .. "lua/menu_localization.lua")

dofile(ModPath .. "automenubuilder.lua")

ChinStringFixes = ChinStringFixes or {
	settings = {
		perk_deck_tip = 1,
		others_tip = true,
		Resmod_Compat = true,
		Enable_String = true
	},
  
    values = {
        perk_deck_tip = { "chinsf_dialog_ovk", "chinsf_dialog_sig", "chinsf_dialog_ovkplussig", "chinsf_dialog_origin", "chinsf_dialog_LtyR" }
    },
    order = {
    	perk_deck_tip = 10,
		others_tip = 11,
		Resmod_Compat = 12,
		Enable_String = 13
    }
  }

AutoMenuBuilder:load_settings(ChinStringFixes.settings, "chin_string_fixes")
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusChinStringFixes", function(menu_manager, nodes)
	
	AutoMenuBuilder:create_menu_from_table(nodes, ChinStringFixes.settings, "chin_string_fixes", "blt_options", ChinStringFixes.values, ChinStringFixes.order)
end)

dofile(ModPath .. "lua/menu_localization.lua")


local schinese = Idstring("schinese"):key() == SystemInfo:language():key()
if not schinese then
	return
end

if not restoration then
	return
end

if not ChinStringFixes.settings.Resmod_Compat then
	return
end

if not ChinStringFixes.settings.Enable_String then
	return
end


--log("Restoration found, running compat")
dofile(ModPath .. "resloc/loczh.lua")

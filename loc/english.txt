{
    "menu_chin_string_fixes" : "Chinese Localization String Fixes",
	"menu_chin_string_fixes_desc" : "Fixes the wrong Chinese description in game.",
	"menu_chin_string_fixes_Enable_String" : "Enable",
	"menu_chin_string_fixes_Enable_String_desc" : "Enable or disable the mod.",
	"menu_chin_string_fixes_perk_deck_tip" : "How to show perk deck?",
	"menu_chin_string_fixes_others_tip" : "Other String",
	"menu_chin_string_fixes_others_tip_desc" : "This contains most of wrong string fixed by official.\nCheck this to display how this mod fixed those string before.",
	"menu_chin_string_fixes_Resmod_Compat" : "Chinese for Restoration MOD",
	"menu_chin_string_fixes_Resmod_Compat_desc" : "This setting makes no effect when your game language is English.",
	"chinsf_dialog_ovk" : "Official",
	"chinsf_dialog_sig" : "Unite",
	"chinsf_dialog_ovkplussig" : "Official/Unite",
	"chinsf_dialog_origin" : "Original PDLG",
	"chinsf_dialog_LtyR" : "Author's Preference",
	"menu_chin_string_fixes_perk_deck_tip_desc" : "Official: 官方中文\nUnite: 联合汉化"
}
